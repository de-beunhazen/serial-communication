#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <stdlib.h>


void write(uint8_t data)
{
	/* Wait for empty transmit buffer */
	loop_until_bit_is_set(UCSR0A,UDRE0);
	
	/* Put data into buffer, sends the data */
	UDR0 = data;
}

uint8_t read(void)
{
	/* Wait for data to be received */
	loop_until_bit_is_set(UCSR0A,RXC0);
	/* Get and return received data from buffer */
	return UDR0;
}

void identify()
{
	write(id); // Control unit with temperature sensor. 
}

void readSensor() 
{
	write(adcValue); 
}

void pullIn() 
{
	manualRollIn();
	write(0x01);
}

void pullOut()
{
	manualRollOut();
	write(0x01);
}

void setThresholdMinADC(){
	uint8_t newValue = read();
	
	if (id == TDR)
	{
		//convert temperature in C to 8bit
		thresholdMinADC = celsiusToBits(newValue);
	}
	if (id == LDR)
	{
		thresholdMinADC = percentageToBits(newValue);
	}
    //thresholdMinADC = newValue;
	
	write(newValue); // Send back the new value
}

void setThresholdMaxADC(){
	uint8_t newValue = read();
		if (id == TDR)
		{
			//convert temperature in C to 8bit
			thresholdMaxADC = celsiusToBits(newValue);
		}
		if (id == LDR)
		{
			thresholdMaxADC = percentageToBits(newValue);
		}
/*	thresholdMaxADC = newValue;*/
	write(newValue); // Send back the new value
}

void setMaxRollOutDistance(){
	uint8_t newValue = read();
	maxRollOutDistance = newValue;
	write(newValue); // Send back the new value
}