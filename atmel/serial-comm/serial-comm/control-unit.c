/*
  * LDR_control-unit.c
  *
  * Created: 29/10/2019 15:07:30
  *  Author: larsp & Maurice Wijker
	* pinout:
	  2 - LED ROOD
	  3 - LED GEEL
	  4 - LED GROEN
	  A5 - AnInput
	  8 - Trigger UltrasoonSensor
	  9 - Echo UltrasoonSensor
	  (Formula: uS / 58 = centimeters)
*/ 
 


#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>


#define TDR 0x01
#define LDR 0x02

const uint8_t id = TDR; //used to identify control unit 0x01 has an TDR 0x02 has en LDR

//initial values for lowering/raising sunscreen
uint8_t thresholdMinADC;//raise sunscreen when ldr/tdr is smaller then this value
uint8_t thresholdMaxADC;//lower sunscreen when ldr/tdr is larger then this value

uint8_t sunscreenMoving = 0; // 0 = not moving | 1 = moving down | 2 = moving up
uint8_t maxRollOutDistance = 20;//in cm
const uint8_t completelyRolledInDistance = 5;//this value reached or lower means sunscreen is completely rolled in. Reason --> ulrasonic is never 0

uint8_t adcValue = 0;
uint8_t adcCompareValue = 0; //use this var to compare with threshold. adcValue will be converted to different units.
uint8_t distance = 0;

#define HIGH 0x1
#define LOW  0x0

//shorthand for pins
const uint8_t redLed = 2;//pin2
const uint8_t yellowLed = 3;//pin3
const uint8_t greenLed = 4;//pin4

const uint8_t trig = 0;//pin8
const uint8_t echo = 1;//pin9

/*uint8_t current_distance = 0;*/



unsigned long cyclesToMicroSeconds(unsigned long cycles){
	return cycles / (F_CPU/1000000);
}

unsigned long getPulseDuration(){
	unsigned long width = 0;
	unsigned long timeout = 1000000;
	
	//Make sure no echo pulse is being send
	PORTB &= ~(_BV(trig));
	_delay_ms(5);
	
	//Send a pulse for 10 micro seconds
	PORTB |= _BV(trig);
	_delay_us(12);

	//Stop sending pulse
	PORTB &= ~(_BV(trig));

	
	//Continue when pulse is low
	while((PINB & _BV(echo))){
		timeout--;
		if (timeout == 0) return 0;
	}
	
	//Continue when pulse is high
	while(!(PINB & _BV(echo))){
		timeout--;
		if (timeout == 0) return 0;
	}
	
	//count while pulse is high
	while(PINB & _BV(echo)){
		if (timeout-- == 0)
		return 0;
		width++;
	}
	
	return cyclesToMicroSeconds(width * 12 + 16);
}

uint8_t percentageToBits(uint8_t percentage){
	return round(percentage * 2.55);
}

uint8_t bitsToCelsius(uint8_t bits){
	double tempC = bits * 0.6375;
	return round(tempC);//this round makes measuring error to high!
}

uint8_t celsiusToBits(uint8_t degrees){
	double bits = degrees / 0.6375;
	return round(degrees);//this round makes measuring error to high!
}

uint8_t bitsToPercentage(uint8_t bits){
	double lightintensity = (double)bits / 255 * 100;
	return round(lightintensity);
}

void initControlUnit(){
	//give initial values based on which sensor is being used
	if(id == TDR)
	{
		//convert to 8 bit representation of temperature
// 		thresholdMinADC = 38;
// 		thresholdMaxADC = 39;
		thresholdMinADC = celsiusToBits(25);
		thresholdMaxADC = celsiusToBits(26);
	}
	if(id == LDR){
		//convert percentage to 8 bit representation
		thresholdMinADC = percentageToBits(5);
		thresholdMaxADC = percentageToBits(35);
	}

	//REFS0 sets AVcc(5V) as reference | MUX0 and MUX2 sets adc5(pin5) as input
	ADMUX = (1 << REFS0) | (1 << MUX0) | (1 << MUX2)|(1<<ADLAR);
	//ADEN enables ADC |  ADPS sets prescaler to 128
	ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
	//disables the digital ability of analog pin 5
	DIDR0 = (1 << ADC5D);
	
	//set pin in/out 
	DDRB = (1<<trig)| (0 << echo);
	DDRD = (1 << PORTD2) | (1 << PORTD3) | (1 << PORTD4);
	
	//set greenLed high. Sunscreen is rolled in at start.
	PORTD |= _BV(greenLed);
}

void readControlUnit(void)
{	
	//start ADC conversion
	ADCSRA |= (1 << ADSC);
	  
	
	readSensorValue();//reads the curvalue of sensor
	updateDistance();//update distance
	moveSunscreen();//move suncreen if conditions are met	      
}

//read value of sensor.
void readSensorValue(){
	//read adc value
	adcCompareValue = ADCH; //use uint8_t to compare with threshold which is also uint8_t
	
	//convert to unit of measurement for the different sensors
	if (id == TDR)
	{
		adcValue = bitsToCelsius(ADCH);
	}else if(id == LDR){
		adcValue = bitsToPercentage(ADCH);
	}
}

void updateDistance(){
		unsigned long width = getPulseDuration();
		distance = width *  0.01717; // converteer tijd naar afstand (cm)
}

void moveSunscreen(){
		//sunscreen is moving blink yellow led
		if ((adcCompareValue >= thresholdMaxADC) && !(PORTD & (1<<redLed)) || (sunscreenMoving == 1))
		{
			PORTD &= ~(_BV(greenLed));
			
			sunscreenMoving = 1; //sets state to rolling out
			
			if(distance < maxRollOutDistance){
				PORTD |= _BV(yellowLed);
				_delay_ms(500);
				PORTD &= ~(_BV(yellowLed));
				_delay_ms(500);
				updateDistance();
			} else if (distance > maxRollOutDistance) {
				updateDistance();
				PORTD &= ~(_BV(yellowLed));
				PORTD |= _BV(redLed);//sunscreen rolled out completely
				sunscreenMoving = 0;
			}
		}
		else if((adcCompareValue <= thresholdMinADC) && !(PORTD & (1<<greenLed)) || (sunscreenMoving == 2)){
			//roll in until sunscreen is completely rolled in
			
			PORTD &= ~(_BV(redLed));
			
			sunscreenMoving = 2; //sets state to rolling in
			
			//while sunscreen is rolling in
			if(distance > completelyRolledInDistance){
				PORTD |= _BV(yellowLed);
				_delay_ms(500);
				PORTD &= ~(_BV(yellowLed));
				_delay_ms(500);
				updateDistance();
			}else if(distance < completelyRolledInDistance){
				updateDistance();
				PORTD &= ~(_BV(yellowLed));
				PORTD |= _BV(greenLed);//sunscreen rolled in completely
				sunscreenMoving = 0;
			}
		} 
}


void manualRollOut(){
	//check if sunscreen is already rolled out
	if (!(PORTD & (1<<redLed)))
	{
	PORTD &= ~(_BV(greenLed));
	for(int i=0; i<5; i++){
		PORTD |= _BV(yellowLed);
		_delay_ms(500);
		PORTD &= ~(_BV(yellowLed));
		_delay_ms(500);
		updateDistance();
	}
	PORTD |= _BV(redLed);//sunscreen rolled out completely
	_delay_ms(5000);
	}	
}

void manualRollIn(){
	//check if suncreen is already rolled in 
	if(!(PORTD & (1<<greenLed))){
	PORTD &= ~(_BV(redLed));
	
	for(int i=0; i<5; i++){
		PORTD |= _BV(yellowLed);
		_delay_ms(500);
		PORTD &= ~(_BV(yellowLed));
		_delay_ms(500);
		
		updateDistance();
	}
	PORTD |= _BV(greenLed);//sunscreen rolled out completely
	_delay_ms(5000);
	}	
}